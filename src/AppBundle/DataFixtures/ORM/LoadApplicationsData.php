<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Applications;
use AppBundle\Entity\Doctor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadApplicationsData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $applications = [];
        for ($i = 0; $i < 40; $i++) {
            $applications[] = [
                'name' => 'Пациент-1',
                'email' => 'frozenpath13@gmail.com',
                'phone' => '3254543545',
                'description' => 'Тестовое обращение пациента',
                'examined' => false,
                'date' => new \DateTime('2017-10-05')
            ];
        }

        foreach ($applications as $value) {
            $application = new Applications();
            $application
                ->setName($value['name'])
                ->setEmail($value['email'])
                ->setPhone($value['phone'])
                ->setDescription($value['description'])
                ->setExamined($value['examined'])
                ->setDate($value['date']);
            $manager->persist($application);
        }

        $application_uniq = new Applications();
        $application_uniq
            ->setName('Заявка для тестирования')
            ->setEmail('frozenpath13@gmail.com')
            ->setPhone(12312321321)
            ->setDescription('Тестовая заявка для автоматизированного тестирования')
            ->setExamined(false)
            ->setDate(new \DateTime());
        $manager->persist($application_uniq);

        $manager->flush();
    }
}