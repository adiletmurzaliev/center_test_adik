<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SliderAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('slider_image', null, array('label' => 'Изображение'))
            ->add('published', null, array('label' => 'Публиковать'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('slider_image', null, array('label' => 'Изображение'))
            ->add('published', null, array('label' => 'Публиковать'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Изображение для слайдера')
            ->add('title', TextType::class, [
                'label' => 'Заголовок изображения'
            ])
            ->add('imageFile', FileType::class, [
                'label' => false
            ])
            ->add('published', CheckboxType::class, [
                'label' => 'Публиковать',
                'label_attr' => ['id' => 'id_published'],
                'required' => false,
            ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Слайдер')
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('slider_image', null, array('label' => 'Изображение'))
            ->add('published', null, array('label' => 'Публиковать'));
    }
}
