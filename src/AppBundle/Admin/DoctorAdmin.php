<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Doctor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Swift_Message;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DoctorAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, array('label' => 'Логин'))
            ->add('email', null, array('label' => 'Электронный адрес'))
            ->add('name', null, array('label' => 'Ф.И.О.'))
            ->add('position', null, array('label' => 'Регалии'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Ф.И.О.'))
            ->add('username', null, array('label' => 'Логин'))
            ->add('email', null, array('label' => 'Электронный адрес'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    public function encodePassword($doctor)
    {
        /** @var Doctor $doctor */
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $password = $container->get('security.password_encoder')
            ->encodePassword($doctor, $doctor->getPassword());
        $doctor->setPassword($password);
        $em->persist($doctor);
        $em->flush();
    }

    public function postPersist($doctor)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $message = Swift_message::newInstance();
        $message->setSubject('Создан аккаунт ' . $doctor->getName())
            ->setFrom('test.luch.center@gmail.com')
            ->setTo($doctor->getEmail())
            ->setBody("Создан новый аккаунт на сайте Центр Лучевой Терапии \n
            По вопросам изменения каких-либо данных необходимо обратиться к администратору.
            Логин: {$doctor->getUsername()}\n
            Пароль: {$doctor->getPassword()}\n
            Ссылка для входа: http://{$_SERVER['HTTP_HOST']}/login");
        $container->get('mailer')->send($message);

        $this->encodePassword($doctor);
    }

    public function postUpdate($doctor)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $message = Swift_message::newInstance();
        $message->setSubject("Аккаунт {$doctor->getName()} был изменен")
            ->setFrom('test.luch.center@gmail.com')
            ->setTo($doctor->getEmail())
            ->setBody("Ваш аккаунт был изменен администратором \n
            По вопросам изменения каких-либо данных необходимо обратиться к администратору.
            Логин: {$doctor->getUsername()}\n
            Пароль: {$doctor->getPassword()}\n
            Ссылка для входа: http://{$_SERVER['HTTP_HOST']}/login");
        $container->get('mailer')->send($message);

        $this->encodePassword($doctor);
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Врач')
            ->add('name', TextType::class, [
                'label' => 'Ф.И.О.'
            ])
            ->add('username', TextType::class, [
                'label' => 'Логин'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Электронный адрес'
            ])
            ->add('enabled', 'hidden', array('data' => true))
            ->add('password', PasswordType::class, [
                'label' => 'Пароль'
            ])
            ->add('role', ChoiceType::class, array(
                'label' => 'Роль',
                'choices' => array(
                    'Врач' => 'ROLE_DOCTOR',
                    'Главный врач' => 'ROLE_SUPER_DOCTOR'
                ),
                'multiple' => false
            ))
            ->add('position', TextareaType::class, [
                'label' => 'Регалии'
            ])
            ->add('biography', TextareaType::class, [
                'label' => 'Биография'
            ])
            ->add('imageFile', FileType::class, array(
                'label' => 'Фотография врача',
                'label_attr' => ['id' => 'id_file'],
                'required' => false));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Врач')
            ->add('name', null, array('label' => 'Ф.И.О.'))
            ->add('username', null, array('label' => 'Логин'))
            ->add('email', null, array('label' => 'Электронный адрес'))
            ->add('position', null, array('label' => 'Регалии'))
            ->add('role', null, array('label' => 'Роль'))
            ->add('biography', null, array('label' => 'Биография'))
            ->add('photo', null, array(
                'label' => 'Фото',
                'template' => 'AppBundle:Admin:list_image.html.twig'
            ));
    }
}
