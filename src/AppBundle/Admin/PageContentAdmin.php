<?php

namespace AppBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PageContentAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('content', null, array('label' => 'Текст'))
            ->add('page', null, array('label' => 'Страница'))
            ->add('published', null, array('label' => 'Публиковать'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('content', null, array('label' => 'Текст'))
            ->add('published', null, array('label' => 'Публиковать'))
            ->add('page', null, array('label' => 'Страница'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Контент для страницы')
            ->add('title', TextType::class, [
                'label' => 'Заголовок'
            ])
            ->add('content', CKEditorType::class, array(
                'label' => 'Содержание',
                'config' => array('toolbar' => 'standard'),
            ))
            ->add('published', ChoiceType::class, [
                'label' => 'Публиковать',
                'choices' => ['да' => true, 'нет' => false],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('page', ChoiceType::class, array('choices' => array(
                'Главная' => 'Главная',
                'Как стать инвестором' => 'Как стать инвестором',
                'Контакты' => 'Контакты'),
                'label' => 'Выбрать страницу',));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Контент для страницы')
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('content', null, array('label' => 'Текст'))
            ->add('page', null, array('label' => 'Страница'))
            ->add('published', null, array('label' => 'Публиковать'));
    }
}
