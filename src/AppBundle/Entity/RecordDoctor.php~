<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * RecordDoctor
 *
 * @ORM\Table(name="record_doctor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordDoctorRepository")
 * @Vich\Uploadable()
 */
class RecordDoctor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="record", type="text")
     */
    private $record;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Patient", inversedBy="recordsDoctor")
     */
    private $patient;

    /**
     * @var string
     *
     * @ORM\Column(name="doctor_file", type="text", length=255, nullable=true)
     */
    private $doctor_file;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="record_doctor_file", fileNameProperty="doctor_file")
     *
     * @var File
     */
    private $differentFile;

    public function setPatient(Patient $patient)
    {
        $this->patient = $patient;
    }

    public function getPatient()
    {
        return $this->patient;
    }



    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return RecordDoctor
     */
    public function setDifferentFile(File $file = null)
    {
        $this->differentFile = $file;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getDifferentFile()
    {
        return $this->differentFile;
    }


    public function __toString()
    {
        return $this->doctor_file ?: '';
    }



    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Doctor", inversedBy="recordsDoctor")
     */
    private $doctor;

    public function setDoctor(Doctor $doctor)
    {
        $this->doctor = $doctor;
    }

    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set record
     *
     * @param string $record
     *
     * @return RecordDoctor
     */
    public function setRecord($record)
    {
        $this->record = $record;

        return $this;
    }

    /**
     * Get record
     *
     * @return string
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return RecordDoctor
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set doctorFile
     *
     * @param string $doctorFile
     *
     * @return RecordDoctor
     */
    public function setDoctorFile($doctorFile)
    {
        $this->doctor_file = $doctorFile;

        return $this;
    }

    /**
     * Get doctorFile
     *
     * @return string
     */
    public function getDoctorFile()
    {
        return $this->doctor_file;
    }
}
