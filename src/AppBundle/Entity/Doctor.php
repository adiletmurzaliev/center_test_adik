<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="doctor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DoctorRepository")
 * @Vich\Uploadable
 */
class Doctor extends User
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="text")
     */
    private $biography;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Schedule", mappedBy="doctor")
     */
    private $schedules;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="doctor_photos", fileNameProperty="photo")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Patient", mappedBy="doctor")
     */
    private $patients;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RecordDoctor", mappedBy="doctor")
     */
    private $recordsDoctor;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string")
     */
    private $position;


    public function __construct()
    {
        $this->recordsDoctor = new ArrayCollection();
        $this->patients = new ArrayCollection();
        $this->schedules = new ArrayCollection();
    }

    public function getRecordsDoctors()
    {
        return $this->recordsDoctor;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Doctor
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->name ?: '';
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Doctor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Doctor
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set biography
     *
     * @param string $biography
     *
     * @return Doctor
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * Get biography
     *
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Add recordsDoctor
     *
     * @param \AppBundle\Entity\RecordDoctor $recordsDoctor
     *
     * @return Doctor
     */
    public function addRecordsDoctor(\AppBundle\Entity\RecordDoctor $recordsDoctor)
    {
        $this->recordsDoctor[] = $recordsDoctor;

        return $this;
    }

    /**
     * Remove recordsDoctor
     *
     * @param \AppBundle\Entity\RecordDoctor $recordsDoctor
     */
    public function removeRecordsDoctor(\AppBundle\Entity\RecordDoctor $recordsDoctor)
    {
        $this->recordsDoctor->removeElement($recordsDoctor);
    }

    /**
     * Get recordsDoctor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecordsDoctor()
    {
        return $this->recordsDoctor;
    }

    public function getRole()
    {
        $role = $this->roles[0];
        return $role;
    }

    public function setRole($role)
    {
        $this->setRoles(array($role));
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Doctor
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Add patient
     *
     * @param \AppBundle\Entity\Patient $patient
     *
     * @return Doctor
     */
    public function addPatient(\AppBundle\Entity\Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param \AppBundle\Entity\Patient $patient
     */
    public function removePatient(\AppBundle\Entity\Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patient
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * Add schedule
     *
     * @param \AppBundle\Entity\Schedule $schedule
     *
     * @return Doctor
     */
    public function addSchedule(\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedules[] = $schedule;

        return $this;
    }

    /**
     * Remove schedule
     *
     * @param \AppBundle\Entity\Schedule $schedule
     */
    public function removeSchedule(\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedules->removeElement($schedule);
    }

    /**
     * Get schedule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }
}
