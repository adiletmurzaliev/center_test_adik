<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Applications;
use AppBundle\Entity\AppResponse;
use AppBundle\Entity\Doctor;
use AppBundle\Entity\Patient;
use AppBundle\Entity\RecordDoctor;
use AppBundle\Entity\Schedule;
use AppBundle\Entity\User;
use AppBundle\Form\AddDoctorType;
use AppBundle\Form\AddPatientType;
use AppBundle\Form\RecordDoctorType;
use AppBundle\Form\SendEmailType;
use DateTime;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

class HospitalController extends Controller
{
    /**
     * @Route("/patients")
     */
    public function patientsAction()
    {
        $patients = $this->getDoctrine()
            ->getRepository('AppBundle:Patient')
            ->findAll();

        return $this->render('@App/Hospital/patients.html.twig', array(
            'patients' => $patients
        ));
    }


    /**
     * @Route("/applications")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function applicationsAction(Request $request)
    {
        $applications = $this->getDoctrine()
            ->getRepository('AppBundle:Applications')
            ->getAplicationsOrderBy();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $applications,
            $request->query->getInt('page', 1)/*page number*/,
            20/*limit per page*/
        );

        return $this->render('@App/Hospital/applications.html.twig', array(
            'applications' => $applications,
            'pagination' => $pagination
        ));
    }


    /**
     * @Route("/findApplications")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findApplicationsAction(Request $request)
    {
        $session = $request->getSession();
        if ($request->isMethod('POST') && $session->get('date')) {
            $session->set('date', null);
        }

        if (!$session->get('date')) {
            $date = new \DateTime($request->request->get('findDateApplication'));
            $session->set('date', $date);
        } else {
            $date = $session->get('date');
        }

        $applications = $this->getDoctrine()
            ->getRepository('AppBundle:Applications')
            ->findApplicationDate($date);

        $quantity = count($applications);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $applications,
            $request->query->getInt('page', 1)/*page number*/,
            20/*limit per page*/
        );

        return $this->render('@App/Hospital/resultFindApplications.html.twig', array(
            'applications' => $applications,
            'pagination' => $pagination,
            'date' => $date,
            'quantity' => $quantity
        ));
    }

    /**
     * @Route("/findData")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findDataAction(Request $request)
    {
        $data = $request->request->get(trim('data'));
        $finder1 = $this->container->get('fos_elastica.finder.app.news');
        $finder2 = $this->container->get('fos_elastica.finder.app.doctor');

        $result_news = $finder1->find($data);
        $result_doctor = $finder2->find($data);

        $record_news = $this->getDoctrine()->getRepository('AppBundle:News')
            ->findNewsData($data);

        $record_doctor = $this->getDoctrine()->getRepository('AppBundle:Doctor')
            ->findDoctorData($data);

        return $this->render('@App/Hospital/searchResult.html.twig', array(
            'record_news' => $record_news,
            'record_doctor' => $record_doctor,
            'result_news' => $result_news,
            'result_doctor' => $result_doctor,
            'data' => $data,
        ));
    }

    /**
     * @Route("/findPatient")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function findPatientAction(Request $request)
    {
        $data = $request->request->get('data');
        $patient = $this->getDoctrine()->getRepository('AppBundle:Patient')
            ->findPatientData($data);

        return $this->render('@App/Hospital/resultFindPatient.html.twig', array(
            'patient' => $patient,
            'data' => $data,
        ));
    }

    /**
     * @Route("/applications/{application_id}")
     * @Method({"GET"})
     * @param $application_id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailApplicationsAction(int $application_id)
    {
        $application = $this->getDoctrine()->getRepository(Applications::class)
            ->find($application_id);

        $application_response = $this->getDoctrine()
            ->getRepository(AppResponse::class)
            ->findOneBy(['application' => $application_id]);

        $form = $this->createForm(SendEmailType::class);

        $application->setExamined(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($application);
        $em->flush();

        return $this->render('@App/Hospital/oneapplications.html.twig', array(
            'application' => $application,
            'application_response' => $application_response,
            'form_popup' => $form->createView()
        ));
    }

    /**
     * @Route("/get-doctors")
     */
    public function getDoctorsAction()
    {
        $data = [];

        $doctors = $this->getDoctrine()
            ->getRepository('AppBundle:Doctor')
            ->findAll();

        foreach ($doctors as $doctor) {
            $data[] = [
                'name' => $doctor->getName(),
            ];
        }
        return new JsonResponse($data);
    }

    /**
     * @Route("/add_doctor/{patient_id}")
     * @param Request $request
     * @param $patient_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addDoctorAction(Request $request, $patient_id)
    {
        $patient = $this->getDoctrine()->getRepository(Patient::class)
            ->find($patient_id);

        $doctor_id = $request->get('app_bundle_add_doctor_type');

        $doctor = $this->getDoctrine()->getRepository(Doctor::class)
            ->find($doctor_id['doctor']);

        $patient->setDoctor($doctor);
        $em = $this->getDoctrine()->getManager();
        $em->persist($patient);
        $em->flush();

        return $this->redirectToRoute('app_hospital_patientprofile', ['id' => $patient_id]);
    }

    /**
     * @Route("/patients/add_patient")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPatientAction(Request $request)
    {
        $patient = new Patient();
        $form = $this->createForm(AddPatientType::class, $patient);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $patient = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($patient);
            $em->flush();

            return $this->redirectToRoute('app_hospital_patients');
        }
        return $this->render('AppBundle:Hospital:add_patient.html.twig', array(
            'form' => $form->createView(),
            'patient' => $patient,
        ));
    }


    /**
     * Displays a form to edit an existing patient entity.
     *
     * @Route("/{id}/edit", name="patient_edit")
     * @Method({"GET","HEAD", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Patient $patient)
    {
        $editForm = $this->createForm('AppBundle\Form\AddPatientType', $patient);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('app_hospital_patientprofile', array('id' => $patient->getId()));
        }
        return $this->render('AppBundle:Hospital:patient_edit.html.twig', array(
            'patient' => $patient,
            'edit_form' => $editForm->createView(),
            'asd' => $editForm->getData(),
        ));
    }


    /**
     * @Route("/patients/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patientProfileAction(Request $request, int $id)
    {
        $doctor = $this->getUser();
        $patient = $this->getDoctrine()
            ->getRepository('AppBundle:Patient')
            ->find($id);

        $add_doctor_form = $this->createForm(AddDoctorType::class);

        $records_doctor = $this->getDoctrine()
            ->getRepository('AppBundle:RecordDoctor')->findRecordDoctorPatientId($id);
        $record = new RecordDoctor();
        $form = $this->createForm(RecordDoctorType::class, $record);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $record->setPatient($patient);
            $record->setDate(new \DateTime());
            $record->setDoctor($doctor);
            $em = $this->getDoctrine()->getManager();
            $em->persist($record);
            $em->flush();
            return $this->redirectToRoute('app_hospital_patientprofile', [
                'id' => $id
            ]);
        }
        return $this->render('@App/Hospital/patient_profile.html.twig', array(
            'patient' => $patient,
            'form' => $form->createView(),
            'records_doctor' => $records_doctor,
            'add_form' => $add_doctor_form->createView(),
            'doctor' => $doctor
        ));
    }

    /**
     * @Route("/personal-area")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function personalAreaAction(Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_main_index');
        }

        /** @var User $user */
        $user = $this->getUser();

        /** @var Doctor $doctor */
        $doctor = new Doctor();

        $schedules = $doctor->getSchedules();

        $date = new \DateTime($request->request->get('schedule-date'));
        $date = $date->format('Y-m-d');

        if (in_array('ROLE_DOCTOR', $user->getRoles())) {
            $doctor = $user;
            $schedules = $this->getDoctrine()->getRepository('AppBundle:Schedule')
                ->getSchedules($date, $doctor->getId());


        } elseif (in_array('ROLE_SUPER_DOCTOR', $user->getRoles())) {
            $doctor = $user;
        }

        return $this->render('AppBundle:Hospital:personal_area.html.twig', array(
            'user' => $user,
            'doctor' => $doctor,
            'schedules' => $schedules,
            'date' => $date
        ));
    }

    /**
     * @Route("/find-schedule/{doctor_id}/{date}")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @param $doctor_id
     * @param $date
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findScheduleAction(Request $request, $doctor_id, $date)
    {

        $scheduless = $this->getDoctrine()
            ->getRepository('AppBundle:Schedule')
            ->getSchedules($date, $doctor_id);

        $schedules = [];
        foreach ($scheduless as $schedule) {
            $data['time'] = $schedule['date']->format('H:i');
            $data['id'] = $schedule['id'];
            $data['name'] = $schedule['name'];
            $data['surname'] = $schedule['surname'];
            $data['type'] = $schedule['type'];
            $schedules[] = $data;
        }

        return new JsonResponse($schedules);
    }

    /**
     * @Route("/set-schedule")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setScheduleAction(Request $request)
    {
        $data = json_decode($_POST['data'], true);

        $patient = $this->getDoctrine()
            ->getRepository('AppBundle:Patient')
            ->find($data['patient_id']);

        $doctor = $this->getDoctrine()
            ->getRepository('AppBundle:Doctor')
            ->find($data['doctor_id']);

        $date = new DateTime($data['date_time']);

        $schedules = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Schedule')
            ->getSchedules($data['date'], $data['doctor_id']);

        foreach ($schedules as $schedule) {
            if ($schedule['date']->format('H:i') == $data['time']) {
                $message = 'fail';

                return new JsonResponse($message);
            }
        }

        $schedule = new Schedule();

        $schedule->setDate($date)
            ->setType($data['type'])
            ->setPatient($patient)
            ->setDoctor($doctor);

        $em = $this->getDoctrine()->getManager();
        $em->persist($schedule);
        $em->flush();

        $message = 'Время успешно назначено';

        return $this->redirectToRoute('app_hospital_findschedule', [
            'doctor_id' => $doctor->getId(),
            'date' => $date->format('Y-m-d')
        ]);
    }
}
