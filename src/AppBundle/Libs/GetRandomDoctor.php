<?php

namespace AppBundle\Libs;

use Doctrine\ORM\EntityManager;


class GetRandomDoctor
{
    public $em;

    public function __construct(EntityManager $em = null)
    {
        $this->em = $em;
    }

    public function getRandomDoctor()
    {
        $doctors = $this->em->getRepository('AppBundle:Doctor')->findAll();
        $random_index = rand(0, (count($doctors) - 1));
        $random_doctor = $doctors[$random_index];
        return $random_doctor;
    }

}