# language: ru
@findApplications
Функционал: Тестируем поиск заявок по дате
  @loginUser
  Сценарий: Открыть страницу заявок и по форме поиска найти заявку по дате
    Допустим я перехожу на страницу "app_hospital_applications"
    И я вижу слово "Выберите дату подачи заявки" где\-то на странице
    И я заполняю поле: "findApplication" значением "2017-10-05" там
    И я нажимаю кнопку "Поиск"
    И я вижу слово "Найдено заявок" где\-то на странице